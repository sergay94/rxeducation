//
//  NetworkRouter.swift
//  RxHabit
//
//  Created by Sergey Chvilev on 01.04.2021.
//

import Foundation
import Alamofire

 enum NetworkRouter: URLRequestConvertible {
    
    static let baseURLPath = "https://droid-test-server.doubletapp.ru/api/"
    static let token = "a413c402-5b4c-43da-9ada-90445953e703"

    case allHabit
    case update(habit: [String: Any])
    case delete(uid: String)
    case done(uid: String)
    
    func asURLRequest() throws -> URLRequest {
        let result: ( metod: HTTPMethod, path: String, parameters: Parameters) = {
            switch self {
            case .allHabit:
                return ( .get, "habit", [:])
            case .update(let habbit):
                return (.put, "habit", habbit)
            case .delete(let uid):
                return (.delete, "habit", ["uid": uid])
            case .done(let uid):
                return (.post, "habit_done", ["habit_uid": uid, "date": Int(Date().timeIntervalSince1970)])
            }
        }()
        
        let url = try NetworkRouter.baseURLPath.asURL()
        var urlRequest = URLRequest(url: url.appendingPathComponent(result.path))
        var headers = [String: String]()
        headers["Authorization"] = NetworkRouter.token
        urlRequest.allHTTPHeaderFields = headers
        urlRequest.httpMethod = result.metod.rawValue
        if  urlRequest.httpMethod != HTTPMethod.get.rawValue {
            return try JSONEncoding.default.encode(urlRequest, with: result.parameters)
        }
        return try URLEncoding.default.encode(urlRequest, with: result.parameters)
    }

}
