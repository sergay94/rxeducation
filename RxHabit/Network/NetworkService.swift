//
//  NetworkService.swift
//  RxHabit
//
//  Created by Sergey Chvilev on 01.04.2021.
//

import Foundation
import RxAlamofire
import RxSwift
import Alamofire

class NetworkService {
    
    func sendAllHabitRequest() ->  Observable<HabitList>{
         RxAlamofire.requestJSON(NetworkRouter.allHabit)
//            .debug()
            .flatMapLatest(parse(dataRequest:))
    }
    
    func update(habit: Habit) ->  Observable<(HTTPURLResponse, Any)>{
        let habitListElemet = HabitListElement.from(habit: habit)
       return  RxAlamofire.requestJSON(NetworkRouter.update(habit: habitListElemet.dictionary ?? [:]))
    }
    
    func  delete(uid: String) ->  Observable<DataRequest>{
        RxAlamofire.request(NetworkRouter.delete(uid: uid))
    }
    
    func  done(uid: String) ->  Observable<DataRequest>{
        RxAlamofire.request(NetworkRouter.done(uid: uid))
    }
    
    private func parse<T: Codable>(dataRequest: (HTTPURLResponse, Any)) -> Observable<T> {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: dataRequest.1, options: .prettyPrinted)
            let decodeValue = try JSONDecoder().decode(T.self, from: jsonData)
            return Observable.just(decodeValue)
        } catch {
            return Observable.empty()
        }
        
    }
}
