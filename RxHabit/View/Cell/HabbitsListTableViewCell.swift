//
//  HabbitsListTableViewCell.swift
//  RxHabit
//
//  Created by Sergey Chvilev on 30.03.2021.
//

import UIKit
import RxSwift

class HabbitsListTableViewCell: UITableViewCell {

    @IBOutlet private var titleLabel: UILabel!
    @IBOutlet private var descriptionLabel: UILabel!
    @IBOutlet private var priorityLabel: UILabel!
    @IBOutlet weak var takeActionButton: UIButton!
    
    var takeActionDisposable: Disposable?
    
    var habbitTitle: String? {
        set {
            titleLabel.text = newValue
        }
        get {
            return titleLabel.text
        }
    }
    
    var habbitDescription: String? {
        set {
            descriptionLabel.text = newValue
        }
        get {
            return descriptionLabel.text
        }
    }
    
    var habbitPriority: Int = 0 {
        didSet {
            var prioruirityString = "-"
            switch habbitPriority {
            case 0:
                prioruirityString = "НИЗ"
            case 1:
                prioruirityString = "СРЕД"
            case 2:
                prioruirityString = "ВЫС"
            default:
                break
            }
            priorityLabel.text = prioruirityString
        }
    }
    
}
