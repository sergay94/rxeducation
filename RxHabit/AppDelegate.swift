//
//  AppDelegate.swift
//  RxHabit
//
//  Created by Sergey Chvilev on 30.03.2021.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        let goodHabitVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "habit_list_controller") as HabitsListViewController
        goodHabitVC.isGoodHabits = true
        let badHabitVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "bad_habit_list_controller") as BadHabitListViewController
        
        let goodHabitNavigationVC = UINavigationController(rootViewController: goodHabitVC)
        let badHabitNavigationVC = UINavigationController(rootViewController: badHabitVC)
       
        let goodItem = UITabBarItem()
        goodItem.title = "Good"
        goodHabitNavigationVC.tabBarItem = goodItem
        
        let badItem = UITabBarItem()
        badItem.title = "Bad"
        badHabitNavigationVC.tabBarItem = badItem

        let tabBarController = UITabBarController()
        tabBarController.viewControllers = [goodHabitNavigationVC, badHabitNavigationVC]
        
        window?.rootViewController = tabBarController
        window?.makeKeyAndVisible()
        return true
    }

}

