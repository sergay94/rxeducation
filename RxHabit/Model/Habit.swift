//
//  Habit.swift
//  RxHabit
//
//  Created by Sergey Chvilev on 30.03.2021.
//

import Foundation
import RealmSwift
import RxDataSources

class Habit: Object {
    @objc dynamic var color = 0, count = 0, date: Int = 0
    @objc dynamic var habitDescription: String = ""
    var doneDates:  List<Int> =  List<Int>()
    @objc dynamic var doneCount: Int = 0
    @objc dynamic var frequency = 0, priority: Int = 0
    @objc dynamic var title: String = ""
    @objc dynamic var type: Int = 0
    @objc dynamic var uid: String = ""
    
    var listElement: HabitListElement {
        HabitListElement.from(habit: self)
    }
    
    override static func primaryKey() -> String? {
        return "uid"
    }
    
    static func fromStruct(_ element: HabitListElement) -> Habit {
        Habit(
            color: element.color,
            count: element.count,
            date: element.date,
            habitDescription: element.habitListDescription,
            doneDates: element.doneDates,
            doneCount: element.doneDates.count,
            frequency: element.frequency,
            priority: element.priority,
            title: element.title,
            type: element.type,
            uid: element.uid
        )
    }
    
    convenience  init(color: Int, count: Int, date: Int, habitDescription: String, doneDates: [Int], doneCount: Int, frequency: Int, priority: Int, title: String, type: Int, uid: String) {
        self.init()
        self.color = color
        self.count = count
        self.date = date
        self.doneCount = doneCount
        self.habitDescription = habitDescription
        self.doneDates.append(objectsIn: doneDates)
        self.frequency = frequency
        self.priority = priority
        self.title = title
        self.type = type
        self.uid = uid
    }
    
    
    func getStringPriority() -> String {
        switch priority {
        case 0:
            return "LOW"
        case 1:
            return "MEDIUM"
        default:
            return "HIGH"
        }
    }
}

struct HabitListElement: Codable {
    let color, count, date: Int
    let habitListDescription: String
    let doneDates: [Int]
    let frequency, priority: Int
    let title: String
    let type: Int
    let uid: String
    
    var habit: Habit {
        Habit.fromStruct(self)
    }
    
    static func from(habit: Habit) -> HabitListElement {
        return HabitListElement(
            color: habit.color,
            count: habit.count,
            date: habit.date,
            habitListDescription: habit.habitDescription,
            doneDates: Array(habit.doneDates),
            frequency: habit.frequency,
            priority: habit.priority,
            title: habit.title,
            type: habit.type,
            uid: habit.uid
        )
    }
    
    enum CodingKeys: String, CodingKey {
        case color, count, date
        case habitListDescription = "description"
        case doneDates = "done_dates"
        case frequency, priority, title, type, uid
    }
}

typealias HabitList = [HabitListElement]

extension HabitListElement: IdentifiableType {
    typealias Identity = String
    
    var identity: String {
        uid
    }
}

extension HabitListElement: Equatable {
    static func ==(lhs: HabitListElement, rhs: HabitListElement) -> Bool {
        return (
            lhs.uid == rhs.uid
                && lhs.title == rhs.title
                && lhs.habitListDescription == rhs.habitListDescription
                && lhs.doneDates.count == rhs.doneDates.count
                && lhs.type == rhs.type
                && lhs.priority == rhs.priority
                && lhs.count == rhs.count
        )
    }
}
