//
//  ReamService.swift
//  RxHabit
//
//  Created by Sergey Chvilev on 30.03.2021.
//

import Foundation
import RealmSwift
import RxRealm
import RxSwift

class ReamService {
    private let realm: Realm!
    
    static let shared = ReamService()
    
    private init() {
        realm = try? Realm()
    }
    
    func habitsFromRealm(isGood: Bool, title: String) -> Observable<[Habit]> {
        let predicate1 = NSPredicate(format: "type == %i", isGood ? 0 : 1)
        let predicate2 = NSPredicate(format: "title contains[cd] %@", title)
        let finalPredicate = title.count > 0 ? NSCompoundPredicate(type: .and, subpredicates: [predicate1, predicate2]) : predicate1
        let habits = realm.objects(Habit.self).filter(finalPredicate)
//        let observable = Observable.collection(from: habits)
        let observable = Observable.arrayWithChangeset(from: habits)
            .map{ Array($0.0)}
        return observable
    }
    
    func takeAction(habit: Habit){
        try? realm.write({
            habit.doneCount += 1
            realm.add(habit, update: .modified)
        })
    }
    
    func takeAction(uid: String){
        guard let habit = habit(by: uid).first else {
            return
        }
        try? realm.write({
            habit.doneCount += 1
            habit.doneDates.append(Int(Date().timeIntervalSince1970))
        })
        save(habit: habit).dispose()
    }
    
    func save(habit: Habit) -> Disposable {
        return Observable.of(habit).subscribe(realm.rx.add(update: .modified, onError: nil))
    }
    
    func delete(uid: String) -> Disposable {
        let deleteHabit = habit(by: uid)
        return Observable.of(deleteHabit)
            .debug()
            .subscribe(realm.rx.delete())
    }
    
    private func habit(by uid: String) -> Results<Habit> {
        realm.objects(Habit.self).filter("uid=%@", uid)
    }
    
}
