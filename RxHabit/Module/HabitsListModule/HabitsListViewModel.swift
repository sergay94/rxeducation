//
//  HabitsListViewModel.swift
//  RxHabit
//
//  Created by Sergey Chvilev on 30.03.2021.
//

import Foundation
import RxSwift
import RxCocoa
import RxRelay
import Alamofire


class HabitsListViewModel {
    
    
    let input: Input
    
    struct Input {
        let searchText: BehaviorRelay<String>
        let sortedRelay: BehaviorRelay<Bool>
        let deleteUid: PublishRelay<String>
        let takeUid: PublishRelay<String>
        let networkRelay:  PublishRelay<Void>
    }
    
    init(isGoodHabits: Bool) {
        self.isGoodHabits = isGoodHabits
        
        let searchText = BehaviorRelay<String>(value: "")
        let sortedRelay = BehaviorRelay<Bool>(value: true)
        let deleteUid = PublishRelay<String>()
        let takeUid = PublishRelay<String>()
        let networkRelay = PublishRelay<Void>()
        input = Input(searchText: searchText, sortedRelay: sortedRelay, deleteUid: deleteUid, takeUid: takeUid, networkRelay: networkRelay)
    }
    
    private let reamService = ReamService.shared
    private let networkService = NetworkService()
    private let disponseBug = DisposeBag()
    private let isGoodHabits: Bool
    
    private lazy var habits: Observable<[Habit]> = {
        return self.input.searchText.asObservable()
            .throttle(.seconds(2), scheduler: MainScheduler.instance)
            .distinctUntilChanged()
            .filter{
                $0.count > 2 || $0.count == 0
            }
            .flatMapLatest(realmHabits)
    }()
    
        lazy var sortedHabits: Driver<[Habit]> = {
            return self.input.sortedRelay.asObservable()
                .flatMapLatest(sortHabit)
                .asDriver(onErrorJustReturn: [])
        }()
    
        lazy var serverHabits: Driver<[Habit]> = {
            return input.networkRelay
                .flatMapLatest(getServerHabits(_:))
                .asDriver(onErrorJustReturn: [])
        }()
    
        lazy var deleteHabitObservable:  Observable<DataRequest> = {
            return self.input.deleteUid.asObservable()
                .flatMapLatest(networkService.delete(uid:))
        }()
    
        lazy var takeHabitObservable:  Observable<DataRequest> = {
            return self.input.takeUid.asObservable()
                .flatMapLatest(networkService.done(uid:))
        }()
    
    private func realmHabits(_ filterString: String) -> Observable<[Habit]> {
        return reamService.habitsFromRealm(isGood: isGoodHabits, title: filterString)
    }
    private func getServerHabits(_: Void) -> Observable<[Habit]> {
        return networkService.sendAllHabitRequest()
            .flatMapLatest(transform)
            .do{ [weak self] (habits) in
                habits.forEach{self?.reamService.save(habit: $0).dispose()}
            }
    }
    
    private func transform(_ habitList: HabitList) -> Observable<[Habit]> {
        var transformList: [Habit] = []
        habitList.forEach{
            transformList.append(Habit.fromStruct($0))
        }
        return Observable.just(transformList)
    }
    
    
    private func sortHabit(_ isAscending: Bool) -> Observable<[Habit]> {
        if isAscending {
            return habits
                .map{$0.sorted{$0.date > $1.date}}
        }
        return habits
            .map{$0.sorted{$0.date < $1.date}}
    }
    
    func delete(_ uid: String) {
        reamService.delete(uid: uid)
            .dispose()
    }
    
    func takeAction(habit: Habit) {
        reamService.takeAction(habit: habit)
    }
}

