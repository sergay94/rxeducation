//
//  ViewController.swift
//  RxHabit
//
//  Created by Sergey Chvilev on 30.03.2021.
//

import UIKit
import RxSwift
import RxCocoa

class HabitsListViewController: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var addHabitButton: UIBarButtonItem!
    @IBOutlet weak var sortButton: UIBarButtonItem!
    @IBOutlet weak var noHabitsFoundLabel: UILabel!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    
    private var viewModel = HabitsListViewModel(isGoodHabits: true)
    let refreshControl = UIRefreshControl()

    private let disposeBag = DisposeBag()
    
    var isGoodHabits: Bool = true {
        didSet {
            viewModel = HabitsListViewModel(isGoodHabits: isGoodHabits)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.refreshControl = refreshControl
        bindViewModel()
        bindUI()
        viewModel.input.networkRelay.accept(())
    }
    
    
    private func bindUI() {
        
        tableView.rx.modelSelected(Habit.self)
            .subscribe {  [weak self] in
            self?.openDetailScreen(for: $0.element)
        }
        .disposed(by: disposeBag)
        
        tableView.rx.modelDeleted(Habit.self).subscribe(onNext: { [weak self]  in
            self?.viewModel.input.deleteUid.accept($0.uid)
            self?.viewModel.delete($0.uid)
        })
        .disposed(by: disposeBag)
        
        searchBar.rx.text.orEmpty
            .bind(to: viewModel.input.searchText)
            .disposed(by: disposeBag)
        
        addHabitButton.rx.tap.bind { [weak self] in
            self?.openDetailScreen(for: nil)
        }
        .disposed(by: disposeBag)
        
        sortButton.rx.tap.bind { [weak self] in
            self?.viewModel.input.sortedRelay.accept(!(self?.viewModel.input.sortedRelay.value ?? false))
        }
        .disposed(by: disposeBag)
        
        refreshControl.rx.controlEvent(.valueChanged)
            .asObservable()
            .bind{ (_) in
                self.viewModel.input.networkRelay.accept(())
            }
            .disposed(by: disposeBag)
            
    }
    
    private func bindViewModel() {
        viewModel.serverHabits
            .asObservable()
//            .debug()
            .subscribe({[weak self] _ in
                self?.refreshControl.endRefreshing()
            }).disposed(by: disposeBag)
        
        viewModel.sortedHabits
            .asObservable()
            .distinctUntilChanged()
//            .debug()
            .subscribe({[weak self] event in
                self?.activityIndicatorView.stopAnimating()
                self?.noDataPlaceholder(isDisplyed: event.element?.count ?? 0 < 1)
            }).disposed(by: disposeBag)
        
        viewModel.sortedHabits
            .debug()
            .drive(tableView.rx.items(cellIdentifier: "habbit_cell", cellType: HabbitsListTableViewCell.self)){_, habit, cell in
                cell.habbitTitle = habit.title
                cell.habbitDescription = habit.habitDescription
                cell.habbitPriority = habit.priority
                cell.takeActionDisposable?.dispose()
                cell.takeActionDisposable = cell.takeActionButton.rx.tap
                    .subscribe(onNext: { [weak self]_ in
                        self?.viewModel.input.takeUid.accept(habit.uid)
                        self?.take(habit: habit)
                    })
            }
            .disposed(by: disposeBag)
        
        viewModel.deleteHabitObservable
//            .debug()
            .subscribe(onError: display(error:))
            .disposed(by: disposeBag)
        
        viewModel.takeHabitObservable
//            .debug()
            .subscribe(onError: display(error:))
            .disposed(by: disposeBag)
    }
    
    private func take(habit: Habit) {
        let timesLeftToDo = habit.count - habit.doneCount + 1
        var message: String? = String(habit.doneCount + 1) + " из " + String(habit.count)
        var title = ""
        if isGoodHabits {
            title = timesLeftToDo > 0 ? "Стоит выполнить еще \(timesLeftToDo) раз" : "You are breathtaking!"
            message = timesLeftToDo > 0 ? nil : message
        } else {
            title = timesLeftToDo > 0 ? "Хватит это делать" : "Можете выполнить еще \(timesLeftToDo) раз"
            message = timesLeftToDo < 1 ? nil : message
        }
        let alert = UIAlertController(title: title, message:  message , preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        self.viewModel.takeAction(habit: habit)
    }
    
    private func noDataPlaceholder(isDisplyed: Bool) {
        noHabitsFoundLabel.isHidden = !isDisplyed
    }
    
    private func display(error: Error) {
        let message = error.localizedDescription
        let alert = UIAlertController(title: "ERROR", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    private func openDetailScreen(for habit: Habit?) {
        self.navigationController?.pushViewController(HabitViewController.createModuleInstance(habit: habit), animated: true)
    }
    
}

extension HabitsListViewController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        view.endEditing(true)
    }
    
}
