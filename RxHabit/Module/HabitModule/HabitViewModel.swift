//
//  HabitViewModel.swift
//  RxHabit
//
//  Created by Sergey Chvilev on 31.03.2021.
//

import Foundation
import RxSwift
import RxCocoa


class HabitViewModel {
    private let disposeBag = DisposeBag()
    private let networkService = NetworkService()
    private var habit: Habit
    
    init(habit: Habit) {
        self.habit = habit
    }
    
    func set(habit: Habit) {
        self.habit = habit
    }
    
    lazy var serverSave: Observable<(HTTPURLResponse, Any)> = {
        return networkService.update(habit: habit)
    }()
    
    func saveBD() {
            ReamService.shared.save(habit: habit)
                .dispose()
    }
}
