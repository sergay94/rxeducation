//
//  HabitViewController.swift
//  RxHabit
//
//  Created by Sergey Chvilev on 31.03.2021.
//

import UIKit
import RxSwift
import RxCocoa

class HabitViewController: UIViewController {
    @IBOutlet weak var habitTitleTextField: UITextField!
    @IBOutlet weak var habitDescriptionTextField: UITextField!
    @IBOutlet weak var amountTextField: UITextField!
    @IBOutlet weak var frequencyTextField: UITextField!
    @IBOutlet weak var priorityButton: UIButton!
    @IBOutlet weak var goodButton: RadioButton!
    @IBOutlet weak var badButton: RadioButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    private var habit: Habit?
    private lazy var viewModel: HabitViewModel = {HabitViewModel(habit: habit ?? Habit())}()
    private let disposeBag = DisposeBag()
    private var priority = 0

    
    static func createModuleInstance(habit: Habit?) -> HabitViewController {
        let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "habit_controller") as! HabitViewController
        controller.habit = habit
        return controller
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        goodButton.alternateButton = [badButton]
        badButton.alternateButton = [goodButton]
        
        setDefaultValue()
        bindUI()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    private func setDefaultValue() {
        guard let habit = self.habit else {
            return
        }
        habitTitleTextField.text = habit.title
        habitDescriptionTextField.text = habit.habitDescription
        amountTextField.text = String(habit.count)
        frequencyTextField.text = String(habit.frequency)
        priorityButton.setTitle(habit.getStringPriority(), for: .normal)
        goodButton.isSelected = habit.type == 0
        priority = habit.priority
    }
    
    private func bindUI() {
        saveButton.rx.tap.bind { [weak self] in
            self?.saveButton.isHidden = true
            self?.activityIndicator.startAnimating()
            self?.updateHabitValues(self?.habit?.uid ?? "")
            self?.viewModel.serverSave
//                .debug()
                .subscribe(onNext: { [weak self] (r, json) in
                if let dict = json as? [String: AnyObject] {
                    if let uid = dict["uid"] as? String {
                        self?.updateHabitValues(uid)
                        self?.viewModel.saveBD()
                        self?.navigationController?.popViewController(animated: true)
                    } else {
                        self?.displayError(message: dict["message"] as? String ?? "Unknow error")
                    }
                }
                }, onError: { [weak self] (error) in
                    self?.displayError(message: error.localizedDescription)
            })
            .disposed(by: self?.disposeBag ?? DisposeBag())
        }.disposed(by: disposeBag)
        priorityButton.rx.tap.bind{ [weak self] in
            self?.priorityButtonTap()
        }.disposed(by: disposeBag)
    }
    
    private func priorityButtonTap() {
        
        func alertAction(title: String, tag: Int) {
            priority = tag
            priorityButton.setTitle(title, for: .normal)
        }
        
        let alert = UIAlertController(title: "Select priority", message: nil, preferredStyle: .actionSheet)
        let lowAction = UIAlertAction(title: "LOW", style: .default) { _ in alertAction(title: "LOW", tag: 0) }
        let mediumAction = UIAlertAction(title: "MEDIUM", style: .default){ _ in alertAction(title: "MEDIUM", tag: 1) }
        let highAction = UIAlertAction(title: "HIGH", style: .default){ _ in alertAction(title: "HIGH", tag: 2) }
        alert.addAction(lowAction)
        alert.addAction(mediumAction)
        alert.addAction(highAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    private func updateHabitValues(_ uid: String){
        let habit =  Habit()
        habit.title = habitTitleTextField.text ?? "Default title"
        habit.habitDescription = habitDescriptionTextField.text ?? "Default desription"
        habit.count = Int(amountTextField.text ?? "0") ?? 0
        habit.frequency = Int(frequencyTextField.text ?? "0") ?? 0
        habit.type = goodButton.isSelected ? 0 : 1
        habit.priority = priority
        habit.uid = uid
        habit.date = Int(Date().timeIntervalSince1970)
        viewModel.set(habit: habit)

    }
    
    private func displayError(message: String) {
        saveButton.isHidden = false
        activityIndicator.stopAnimating()
        let alert = UIAlertController(title: "ERROR", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
