//
//  BadHabitListViewController.swift
//  RxHabit
//
//  Created by Sergey Chvilev on 05.04.2021.
//

import UIKit
import RxSwift
import RxCocoa
import RxRelay
import RxDataSources
import Alamofire

class BadHabitListViewController: UIViewController {
    @IBOutlet var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var addHabitButton: UIBarButtonItem!
    @IBOutlet weak var sortButton: UIBarButtonItem!
    @IBOutlet weak var noHabitsFoundLabel: UILabel!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    
    private let viewModel = BadHabitsListViewModel()
    let refreshControl = UIRefreshControl()

    private let disposeBag = DisposeBag()
    
    lazy var dataSource: RxTableViewSectionedAnimatedDataSource<HabitSection> = {
        let dataSource = RxTableViewSectionedAnimatedDataSource<HabitSection>(configureCell: { (_, tableView, indexPath, habit) -> UITableViewCell in
            let cell = tableView.dequeueReusableCell(withIdentifier: "habbit_cell", for: indexPath) as! HabbitsListTableViewCell
            cell.habbitTitle = habit.title
            cell.habbitDescription = habit.habitListDescription
            cell.habbitPriority = habit.priority
            cell.takeActionDisposable?.dispose()
            cell.takeActionDisposable = cell.takeActionButton.rx.tap
                .subscribe(onNext: { [weak self]_ in
                    self?.viewModel.input.takeUid.accept(habit.uid)
                    self?.take(habit: habit)
                })
            return cell
        })
        
        dataSource.animationConfiguration = AnimationConfiguration(insertAnimation: .bottom, reloadAnimation: .none, deleteAnimation: .left)
        dataSource.canEditRowAtIndexPath = {_,_ in
            true
        }
        dataSource.titleForHeaderInSection = { habitSection, index in
             habitSection[index].header
          }
        return dataSource
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.refreshControl = refreshControl
        bindViewModel()
        bindUI()
        viewModel.input.networkRelay.accept(())
    }
    
    
    private func bindUI() {
        
        tableView.rx.modelSelected(HabitSection.Item.self)
            .subscribe {  [weak self] in
                self?.openDetailScreen(for: $0.element?.habit)
        }
        .disposed(by: disposeBag)
        
        tableView.rx.modelDeleted(HabitSection.Item.self).subscribe(onNext: { [weak self]  in
            self?.viewModel.input.deleteUid.accept($0.uid)
            self?.viewModel.delete($0.uid)
        })
        .disposed(by: disposeBag)
        
        searchBar.rx.text.orEmpty
            .bind(to: viewModel.input.searchText)
            .disposed(by: disposeBag)
        
        addHabitButton.rx.tap.bind { [weak self] in
            self?.openDetailScreen(for: nil)
        }
        .disposed(by: disposeBag)
        
        sortButton.rx.tap.bind { [weak self] in
            self?.viewModel.input.sortedRelay.accept(!(self?.viewModel.input.sortedRelay.value ?? false))
        }
        .disposed(by: disposeBag)
        
        refreshControl.rx.controlEvent(.valueChanged)
            .asObservable()
            .bind{ (_) in
                self.viewModel.input.networkRelay.accept(())
            }
            .disposed(by: disposeBag)
            
    }
    
    private func bindViewModel() {
        viewModel.serverHabits
            .asObservable()
            .subscribe({[weak self] _ in
                self?.refreshControl.endRefreshing()
            }).disposed(by: disposeBag)
        
        viewModel.sortedHabits
            .asObservable()
            .subscribe({[weak self] event in
                self?.activityIndicatorView.stopAnimating()
                self?.noDataPlaceholder(isDisplyed: event.element?.first?.items.count ?? 0 < 1)
            }).disposed(by: disposeBag)
        
        viewModel.sortedHabits
            .drive(tableView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)
        
        viewModel.deleteHabitObservable
            .subscribe(onError: display(error:))
            .disposed(by: disposeBag)
        
        viewModel.takeHabitObservable
            .subscribe(onError: display(error:))
            .disposed(by: disposeBag)
    }
    
    private func take( habit: HabitSection.Item) {
        let timesLeftToDo = habit.count - habit.doneDates.count + 1
        var message: String? = String(habit.doneDates.count + 1) + " из " + String(habit.count)
        let title = timesLeftToDo < 0 ? "Хватит это делать" : "Можете выполнить еще \(timesLeftToDo) раз"
        message = timesLeftToDo > 1 ? nil : message
        let alert = UIAlertController(title: title, message:  message , preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        self.viewModel.takeAction(habit.uid)
    }
    
    private func noDataPlaceholder(isDisplyed: Bool) {
        noHabitsFoundLabel.isHidden = !isDisplyed
    }
    
    private func display(error: Error) {
        let message = error.localizedDescription
        let alert = UIAlertController(title: "ERROR", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    private func openDetailScreen(for habit: Habit?) {
        self.navigationController?.pushViewController(HabitViewController.createModuleInstance(habit: habit), animated: true)
    }
    
}

extension BadHabitListViewController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        view.endEditing(true)
    }
    
}
