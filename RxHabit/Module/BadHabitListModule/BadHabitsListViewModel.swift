//
//  BadHabitsListViewModel.swift
//  RxHabit
//
//  Created by Sergey Chvilev on 05.04.2021.
//

import Foundation
import RxSwift
import RxCocoa
import RxRelay
import RxDataSources
import Alamofire


struct HabitSection {
    
    var header: String
    var items: [HabitListElement]
}

class BadHabitsListViewModel {
    
    let input: Input
    
    struct Input {
        let searchText: BehaviorRelay<String>
        let sortedRelay: BehaviorRelay<Bool>
        let deleteUid: PublishRelay<String>
        let takeUid: PublishRelay<String>
        let networkRelay:  PublishRelay<Void>
        let updateUid: PublishRelay<String>
    }
    
    init() {
        let searchText = BehaviorRelay<String>(value: "")
        let sortedRelay = BehaviorRelay<Bool>(value: true)
        let deleteUid = PublishRelay<String>()
        let takeUid = PublishRelay<String>()
        let networkRelay = PublishRelay<Void>()
        let updateUid = PublishRelay<String>()
        input = Input(searchText: searchText, sortedRelay: sortedRelay, deleteUid: deleteUid, takeUid: takeUid, networkRelay: networkRelay, updateUid: updateUid)
    }
    
    private let reamService = ReamService.shared
    private let networkService = NetworkService()
    private let disponseBug = DisposeBag()
    
    private lazy var habits: Observable<[HabitSection]> = {
        return self.input.searchText.asObservable()
            .throttle(.seconds(2), scheduler: MainScheduler.instance)
            .distinctUntilChanged()
            .filter{
                $0.count > 2 || $0.count == 0
            }
            .flatMapLatest(realmHabits)
    }()
    
    lazy var sortedHabits: Driver<[HabitSection]> = {
        return self.input.sortedRelay.asObservable()
            .flatMapLatest(sortHabit)
            .asDriver(onErrorJustReturn: [])
    }()
    
    lazy var serverHabits: Driver<[Habit]> = {
        return input.networkRelay
            .flatMapLatest(getServerHabits(_:))
            .asDriver(onErrorJustReturn: [])
    }()
    
    lazy var deleteHabitObservable:  Observable<DataRequest> = {
        return self.input.deleteUid.asObservable()
            .flatMapLatest(networkService.delete(uid:))
    }()
    
    lazy var takeHabitObservable:  Observable<DataRequest> = {
        return self.input.takeUid.asObservable()
            .flatMapLatest(networkService.done(uid:))
    }()
    
    private func realmHabits(_ filterString: String) -> Observable<[HabitSection]> {
        return reamService.habitsFromRealm(isGood: false, title: filterString)
            .map({ realmHabits in
                var habit = [HabitListElement]()
                realmHabits.forEach{habit.append($0.listElement) }
                return habit
            })
            .map{[HabitSection(header: "Section one", items: $0)]}
    }
    private func getServerHabits(_: Void) -> Observable<[Habit]> {
        return networkService.sendAllHabitRequest()
//            .debug()
            .flatMapLatest(transform)
            .do{ [weak self] (habits) in
                habits.forEach{self?.reamService.save(habit: $0).dispose()}
            }
    }
    
    func transform(_ habitList: HabitList) -> Observable<[Habit]> {
        var transformList: [Habit] = []
        habitList.forEach{
            transformList.append(Habit.fromStruct($0))
        }
        return Observable.just(transformList)
    }
    
    
    func sortHabit(_ isAscending: Bool) -> Observable<[HabitSection]> {
        if isAscending {
            return habits
                .map { (sections) in
                    var sortedSections: [HabitSection] = []
                    sections.forEach { (section) in
                        let sortedSection = HabitSection(original: section, items: section.items.sorted{ $0.date > $1.date})
                        sortedSections.append( sortedSection)
                    }
                    return sortedSections
                }
        }
        return habits
            .map { (sections) in
                var sortedSections: [HabitSection] = []
                sections.forEach { (section) in
                    sortedSections.append(HabitSection(original: section, items: section.items.sorted{ $0.date < $1.date}))
                }
                return sortedSections
            }
    }
    
    func delete(_ uid: String) {
        reamService.delete(uid: uid)
            .dispose()
    }
    
    func takeAction(_ uid: String) {
        reamService.takeAction(uid: uid)
    }
}


extension HabitSection: AnimatableSectionModelType {
    var identity: String {
        header
    }
    
    typealias Identity = String
}

extension HabitSection: SectionModelType {
    typealias Item = HabitListElement
    init(original: HabitSection, items: [Item]) {
        self = original
        self.items = items
    }
}
